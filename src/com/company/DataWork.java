package com.company;
import java.util.*;
import java.io.File;
import java.time.DayOfWeek;
import java.time.LocalDate;

public class DataWork
{
    ArrayList<String[]> data = new ArrayList<>();
    ArrayList<String> spliter = new ArrayList<>();
    String pathname = "E:\\Project\\Semestr 5 Cross-programming\\Lab 4\\data.txt";
    String months[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    Hashtable<String, Integer> monthsAlphabetic = new Hashtable<>();

    public void allDates()
    {
        System.out.println("Original dates list: ");
        for (var elem: data)
        {
            int[] lowestDate = new int[3];
            if(Integer.parseInt(elem[0]) == 1)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[2].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[2].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[2].substring(0,2));
                    }
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[2]);
                    if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3]);
                    }
                    else
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                                break;
                            }
                        }
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
            }
            else if(Integer.parseInt(elem[0]) == 2)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    if(elem[2].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[2].substring(1,2));
                    }
                    else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[2].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[2].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[3]);
                    lowestDate[1] = Integer.parseInt(elem[2]);
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
            }
            else if(Integer.parseInt(elem[0]) == 3)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[4].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[4].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[4].substring(0,2));
                    }
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[2]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[4]);
                    lowestDate[1] = Integer.parseInt(elem[3]);
                    lowestDate[2] = Integer.parseInt(elem[2]);
                }
            }
            LocalDate localDate = LocalDate.of(lowestDate[2], lowestDate[1], lowestDate[0]);
            System.out.println(localDate + " " + localDate.getDayOfWeek());
        }
        System.out.println();
    }

    public void allDatesModified()
    {
        System.out.println("Modified dates list: ");
        for (var elem: data)
        {
            int[] lowestDate = new int[3];
            if(Integer.parseInt(elem[0]) == 1)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[2].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[2].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[2].substring(0,2));
                    }
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[2]);
                    if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3]);
                    }
                    else
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                                break;
                            }
                        }
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
            }
            else if(Integer.parseInt(elem[0]) == 2)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    if(elem[2].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[2].substring(1,2));
                    }
                    else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[2].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[2].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[3]);
                    lowestDate[1] = Integer.parseInt(elem[2]);
                    lowestDate[2] = Integer.parseInt(elem[4]);
                }
            }
            else if(Integer.parseInt(elem[0]) == 3)
            {
                if(Integer.parseInt(elem[1]) == 1)
                {
                    if(elem[4].substring(0,1).equals("0"))
                    {
                        lowestDate[0] = Integer.parseInt(elem[4].substring(1,2));
                    }
                    else
                    {
                        lowestDate[0] = Integer.parseInt(elem[4].substring(0,2));
                    }
                    if(elem[3].substring(0,1).equals("0"))
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                    }
                    else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                    {
                        for (var keys: monthsAlphabetic.keySet())
                        {
                            if(elem[3].equals(keys))
                            {
                                lowestDate[1] = monthsAlphabetic.get(keys);
                            }
                        }
                    }
                    else
                    {
                        lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                    }
                    lowestDate[2] = Integer.parseInt(elem[2]);
                }
                else if(Integer.parseInt(elem[1]) == 2)
                {
                    lowestDate[0] = Integer.parseInt(elem[4]);
                    lowestDate[1] = Integer.parseInt(elem[3]);
                    lowestDate[2] = Integer.parseInt(elem[2]);
                }
            }
            LocalDate localDate = LocalDate.of(lowestDate[2], lowestDate[1], lowestDate[0]+1);
            System.out.println(localDate + " " + localDate.getDayOfWeek());
        }
        System.out.println();
    }

    public void setMonthsAlphabetic()
    {
        for(int i = 1; i <= months.length; i++)
        {
            monthsAlphabetic.put(months[i-1], i);
        }
    }

    public void diapasonSearch()
    {
        int[] lowest = lowestDateFind();
        int[] biggest = biggestDateFind();
        System.out.println("Period is: " + biggest[0] + " " + biggest[1] + " " + biggest[2] + " - " + lowest[0] + " " + lowest[1] + " " + lowest[2]);
    }

    public int[] lowestDateFind()
    {
        int[] lowestDate = new int[5];
        for(var elem: data)
        {
            if(lowestDate[0] == 0)
            {
                if(Integer.parseInt(elem[0]) == 1)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[2]);
                        if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3]);
                        }
                        else
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 2)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[2].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[3]);
                        lowestDate[1] = Integer.parseInt(elem[2]);
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 3)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[4].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[4].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[4].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[2]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[4]);
                        lowestDate[1] = Integer.parseInt(elem[3]);
                        lowestDate[2] = Integer.parseInt(elem[2]);
                    }
                }
            }
            else
            {
                int[] lowestDate2 = new int[3];
                if(Integer.parseInt(elem[0]) == 1)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[2]);
                        if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3]);
                        }
                        else
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 2)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[2].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[3]);
                        lowestDate2[1] = Integer.parseInt(elem[2]);
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 3)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[4].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[4].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[4].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[2]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[4]);
                        lowestDate2[1] = Integer.parseInt(elem[3]);
                        lowestDate2[2] = Integer.parseInt(elem[2]);
                    }
                }
                if(lowestDate[2] < lowestDate2[2])
                {
                    lowestDate = lowestDate2;
                }
                else if(lowestDate[2] == lowestDate2[2])
                {
                    if(lowestDate[1] < lowestDate2[1])
                    {
                        lowestDate = lowestDate2;
                    }
                    else if(lowestDate[1] == lowestDate2[1])
                    {
                        if(lowestDate[0] < lowestDate2[0])
                        {
                            lowestDate = lowestDate2;
                        }
                    }
                }
            }
        }
        return lowestDate;
    }

    public int[] biggestDateFind()
    {
        int[] lowestDate = new int[5];
        for(var elem: data)
        {
            if(lowestDate[0] == 0)
            {
                if(Integer.parseInt(elem[0]) == 1)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[2]);
                        if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3]);
                        }
                        else
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 2)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[2].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[3]);
                        lowestDate[1] = Integer.parseInt(elem[2]);
                        lowestDate[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 3)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[4].substring(0,1).equals("0"))
                        {
                            lowestDate[0] = Integer.parseInt(elem[4].substring(1,2));
                        }
                        else
                        {
                            lowestDate[0] = Integer.parseInt(elem[4].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate[2] = Integer.parseInt(elem[2]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate[0] = Integer.parseInt(elem[4]);
                        lowestDate[1] = Integer.parseInt(elem[3]);
                        lowestDate[2] = Integer.parseInt(elem[2]);
                    }
                }
            }
            else
            {
                int[] lowestDate2 = new int[3];
                if(Integer.parseInt(elem[0]) == 1)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[2]);
                        if(Character.isDigit(elem[3].substring(0,1).toCharArray()[0]))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3]);
                        }
                        else
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                    break;
                                }
                            }
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 2)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        if(elem[2].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[2].substring(1,2));
                        }
                        else if(Character.isLetter( elem[2].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[2].equals(keys))
                                {
                                    lowestDate2[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[2].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[3]);
                        lowestDate2[1] = Integer.parseInt(elem[2]);
                        lowestDate2[2] = Integer.parseInt(elem[4]);
                    }
                }
                else if(Integer.parseInt(elem[0]) == 3)
                {
                    if(Integer.parseInt(elem[1]) == 1)
                    {
                        if(elem[4].substring(0,1).equals("0"))
                        {
                            lowestDate2[0] = Integer.parseInt(elem[4].substring(1,2));
                        }
                        else
                        {
                            lowestDate2[0] = Integer.parseInt(elem[4].substring(0,2));
                        }
                        if(elem[3].substring(0,1).equals("0"))
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(1,2));
                        }
                        else if(Character.isLetter( elem[3].substring(0,1).toCharArray()[0]))
                        {
                            for (var keys: monthsAlphabetic.keySet())
                            {
                                if(elem[3].equals(keys))
                                {
                                    lowestDate[1] = monthsAlphabetic.get(keys);
                                }
                            }
                        }
                        else
                        {
                            lowestDate2[1] = Integer.parseInt(elem[3].substring(0,2));
                        }
                        lowestDate2[2] = Integer.parseInt(elem[2]);
                    }
                    else if(Integer.parseInt(elem[1]) == 2)
                    {
                        lowestDate2[0] = Integer.parseInt(elem[4]);
                        lowestDate2[1] = Integer.parseInt(elem[3]);
                        lowestDate2[2] = Integer.parseInt(elem[2]);
                    }
                }
                if(lowestDate[2] > lowestDate2[2])
                {
                    lowestDate = lowestDate2;
                }
                else if(lowestDate[2] == lowestDate2[2])
                {
                    if(lowestDate[1] > lowestDate2[1])
                    {
                        lowestDate = lowestDate2;
                    }
                    else if(lowestDate[1] == lowestDate2[1])
                    {
                        if(lowestDate[0] > lowestDate2[0])
                        {
                            lowestDate = lowestDate2;
                        }
                    }
                }
            }
        }
        return lowestDate;
    }

    public void readFromFile()
    {
        setMonthsAlphabetic();
        try
        {
            Scanner scanner = new Scanner(new File(("E:\\Project\\Semestr 5 Cross-programming\\Lab 4\\data.txt")));
            String line;
            String[] splitedLine = new String[5];
            while (scanner.hasNextLine())
            {
                line = scanner.nextLine();
                String[] splitters = new String[] {".","-","/"," "};
                for (var elem: splitters)
                {
                    if(line.indexOf(elem, 4) != -1)
                    {
                        spliter.add(elem);
                        break;
                    }
                }
                splitedLine = line.split("\\.|-|/| ");
                data.add(splitedLine);
            }
            scanner.close();
        }
        catch (Exception exception) {
            System.out.println("There is exception in file reading: " + exception);
        }
    }
}
