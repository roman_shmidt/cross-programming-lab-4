package com.company;

public class Main {

    public static void main(String[] args)
    {
        DataWork dataWork = new DataWork();
        dataWork.readFromFile();
        dataWork.diapasonSearch();
        dataWork.allDates();
        dataWork.allDatesModified();
    }
}
